﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SWS;
using UnityEngine;
using UnityEngine.UI;

public class ProvaChangeWithSlider : MonoBehaviour {

    Slider slider;

    /// <summary>
    /// Speed value to multiply the input speed with. 
    /// <summary>
    public float speedMultiplier = 10f;

    /// <summary>
    /// Object progress on the path, should be read only.
    /// <summary>
    public float progress = 0f;

    //references
    private splineMove move;
    private Animator animator;
    public float duration;
    float speed;

    public GameObject Walker;

    // Use this for initialization
    void Start () {
        slider = this.GetComponent<Slider> ();
        animator = Walker.GetComponent<Animator> ();
        move = Walker.GetComponent<splineMove> ();
        move.StartMove ();
        move.Pause ();
        progress = 0f;
    }

    // Update is called once per frame
    void Update () {
        speed = speedMultiplier / 100f;
        duration = move.tween.Duration ();
		//ChangeSlider();
    }

    public void ChangeSlider () {
        progress = slider.value;
        progress = Mathf.Clamp (progress, 0, duration);
        move.tween.fullPosition = progress;
    }

    public float Map (float from, float to, float from2, float to2, float value) {
        if (value <= from2) {
            return from;
        } else if (value >= to2) {
            return to;
        } else {
            return (to - from) * ((value - from2) / (to2 - from2)) + from;
        }
    }
}