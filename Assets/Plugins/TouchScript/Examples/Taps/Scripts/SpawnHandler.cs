﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnHandler : NetworkBehaviour {

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        // Debug.Log ("IS SERVER: " + Network.isServer + " IS CLIENT: " + Network.isClient);

        if (Network.isClient) {
            this.GetComponent<MeshRenderer> ().enabled = false;
            this.transform.GetChild (0).gameObject.SetActive (true);
        }
    }
}