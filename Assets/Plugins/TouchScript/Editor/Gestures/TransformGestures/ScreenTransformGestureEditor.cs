﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using TouchScript.Editor.Gestures.TransformGestures.Base;
using TouchScript.Gestures.TransformGestures;
using UnityEditor;

namespace TouchScript.Editor.Gestures.TransformGestures
{
    [CustomEditor(typeof(ScreenTransformGesture), true)]
    internal class ScreenTransformGestureEditor : TwoPointTransformGestureBaseEditor
    {
    }
}
