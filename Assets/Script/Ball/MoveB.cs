﻿using System;
using System.Collections;
using System.Collections.Generic;
using SWS;
using UnityEngine;

namespace SWS {

    public class MoveB : MonoBehaviour {

        public MoveAnimator moveAnimator { get; set; }

        public Vector3 offset = new Vector3(0,0,0);
        public bool inMano { get; set; }

        // Use this for initialization
        void Start () {
            inMano = false;
        }

        // Update is called once per frame
        void Update () {
            if (inMano) {
                // Debug.Log ("E SCORRANO PALLA IN MANO VA");
                this.transform.position = moveAnimator.GetManoDX().position + offset;
            }
        }
    }

}