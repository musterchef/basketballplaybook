﻿using System;
using System.Collections;
using System.Collections.Generic;
using SWS;
using UnityEngine;

public class MoveBall : MonoBehaviour {

    [SerializeField] private MoveAnimator moveAnimator;
    [SerializeField] private PlayersManager playersManager;
    [SerializeField] private Transform pallaMadre;
    public string parameters { set; get; }

    // Use this for initialization
    void Start () { }

    // Update is called once per frame
    void Update () {

    }

    // metodo per passare la palla
    public IEnumerator LerpBall (Transform t, Vector3 startPos, float time, Transform p, MoveAnimator ma, MoveAnimator map) {

        Debug.Log ("Sono dentro" + ": " + ma.name);

        this.pallaMadre.GetChild(0).GetComponent<MeshRenderer> ().enabled = true;

        double i = 0.0;
        var rate = 1.0 / time;
        while (i < 1.0) {
            if ((i > 0.50d) && (ma.GetPalleggio () == false)) {
                ma.SetLayerW(true);
                Debug.Log ("cambio palleggiatore");
                OnPassaggioFalse();
                Ricezione (ma);
                ma.SetRicezione ();
                // ma.SetPalleggio (true);
            }
            i += Time.deltaTime * rate;
            pallaMadre.position = Vector3.Lerp (startPos, ma.GetMani (), (float) i);
            yield return null;
        }

        map.SetLayerW(false);

        // // disattivo la palla quando arriva nelle mani e attivo quella del giocatore che la riceve ;)

        // this.pallaMadre.GetComponent<MeshRenderer> ().enabled = false;
        // this.pallaMadre.GetChild (0).GetComponent<MeshRenderer> ().enabled = false;
        // p.GetComponent<MeshRenderer> ().enabled = true;
    }

    // metodo chiamando come evento durante l'animazione del passaggio e serve a lanciare il LerpBall per far sì
    // che la palla vada a finire nelle mani del giocatore che è il ricevitore

    public void MetodoEvento () {
        // sistemo i parametri che arrivano tutti dentro una stringa
        string[] ps = parameters.Split (';');
        float duration = float.Parse (ps[2]);
        MoveAnimator map = playersManager.playermap[ps[0]].GetComponent<MoveAnimator> ();
        MoveAnimator ma = playersManager.playermap[ps[1]].GetComponent<MoveAnimator> ();
        Transform p = playersManager.playermap[ps[1]].GetComponent<MoveAnimator> ().palla.transform;
        // spengo la palla del giocatore che sta per passare e accendo quella che deve partire nel LerpBall
        this.GetComponent<Renderer> ().enabled = false;
        pallaMadre.position = playersManager.playermap[ps[0]].GetComponent<MoveAnimator> ().palla.transform.position;
        // Debug.Log ("pallaMadre: " + pallaMadre.position);
        map.SetPalleggio (false);
        StartCoroutine (LerpBall (pallaMadre, pallaMadre.position, duration, p, ma, map));
    }

    public void Ricezione (MoveAnimator ma) {
        MoveB mb = pallaMadre.GetComponent<MoveB> ();
        mb.moveAnimator = ma;
        mb.inMano = true;
    }

    public void OnPassaggioFalse(){
        this.transform.parent.transform.parent.GetComponent<MoveAnimator>().onPassaggio = false;
    }

    // public void InizializzaPassaggio () {
    //     passStarted = true;
    //     this.pallaMadre.GetComponent<Renderer> ().enabled = true;
    //     this.GetComponent<Renderer> ().enabled = false;
    // }
}