﻿using System.Collections;
using System.Collections.Generic;
using SWS;
using UnityEngine;

// Classe per gestire le animazioni del personaggio 3D del giocatore

public class Move3DPlayer : MonoBehaviour {

    public Animator Animator;
    public splineMove splineMove;

    public Ball Ball;
    [SerializeField] private float rotationalDamp = 2f;

    // Use this for initialization
    void Start () { }

    // Update is called once per frame
    void Update () {

        // Imposto la velocità del giocatore come quella del componente splineMove del suo tondino

        if ((splineMove.pathContainer != null)) {
            if ((splineMove.currentPoint == splineMove.waypoints.Length - 1)) {
                this.Animator.SetFloat ("Forward", 0f);
            } else {
                this.Animator.SetFloat ("Forward", splineMove.speed);
            }
        }

        // calcolo la direzione e la passo alla rotazione

        if ((splineMove.waypoints.Length > 0) && ((splineMove.currentPoint != splineMove.waypoints.Length - 1))) {

            Vector3 wp1 = splineMove.waypoints[splineMove.currentPoint];
            Vector3 wp2 = splineMove.waypoints[splineMove.currentPoint + 1];
            Vector3 dir = (wp2 - wp1).normalized;

            // this.gameObject.transform.rotation = Quaternion.LookRotation (dir);

            Quaternion toRot = Quaternion.LookRotation (dir);
            Quaternion curRot = Quaternion.Slerp(this.gameObject.transform.rotation, toRot, Time.deltaTime * rotationalDamp);
            this.gameObject.transform.rotation = curRot;


        }

    }

}