using System.Collections;
using System.Collections.Generic;
using Photon;
using UnityEngine;
using UnityEngine.Networking;

public class Manager3D : Photon.MonoBehaviour {

    //public AnimationManager AnimationManager;
    //public TimelineManager TimelineManager;
    public PlayersManager PlayersManager;

    [SerializeField] private Camera tribunaCamera, defaultCamera;
    private bool treddi;
    public Transform ball;
    // private PhotonView pv;

    // Use this for initialization
    void Start () {
        treddi = false;
        // pv = this.GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update () {

    }

    public void CallEditOnClients () {
        PhotonView photonView = PhotonView.Get (this);
        photonView.RPC ("EditMode", PhotonTargets.OthersBuffered);
    }

    [PunRPC]
    public void EditMode () {
        //accendo la camera in tribuna
        tribunaCamera.gameObject.SetActive (true);
        tribunaCamera.enabled = true;

        //metto i giocatori in 2D visti da alto

        List<Transform> giocatori = PlayersManager.GetHomePlayerInTheSceneList ();
        foreach (Transform t in giocatori) {
            Debug.Log ("ZIOFA");
            t.GetChild (0).gameObject.SetActive (false);
            t.gameObject.GetComponent<MeshRenderer> ().enabled = true;
            // }
        }

        Debug.Log ("EDIT MODE");
    }

    public void CallActivate3DPlayerOnClients () {
        Debug.Log ("IS: " + PhotonNetwork.isMasterClient);
        PhotonView photonView = PhotonView.Get (this);
        // photonView.RPC ("Activate3DPlayerOnClients", PhotonTargets.All);
        photonView.RPC ("Activate3DPlayerOnClients", PhotonTargets.All);
        // ActivateAndDisableMesh ();
    }

    [PunRPC]
    public void Activate3DPlayerOnClients () {
        if(!PhotonNetwork.isMasterClient){
        List<Transform> giocatori = PlayersManager.GetHomePlayerInTheSceneList ();
        foreach (Transform t in giocatori) {
            Debug.Log ("ZIOFA");
            t.GetChild (0).gameObject.SetActive (!t.GetChild (0).gameObject.activeInHierarchy);
            t.gameObject.GetComponent<MeshRenderer> ().enabled = !t.gameObject.GetComponent<MeshRenderer> ().enabled;
            }
        }

        ball.gameObject.GetComponent<MeshRenderer> ().enabled = !ball.gameObject.GetComponent<MeshRenderer> ().enabled;
    }

    private void ActivateAndDisableMesh () {
        List<Transform> giocatori = PlayersManager.GetHomePlayerInTheSceneList ();
        foreach (Transform t in giocatori) {
            t.GetChild (0).gameObject.SetActive (true);

            for (int i = 0; i < t.GetChild (0).childCount; i++) {
                t.GetChild (0).GetChild (i).gameObject.SetActive (false);
            }
        }
    }

}