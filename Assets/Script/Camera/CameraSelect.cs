﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSelect : MonoBehaviour {
    [SerializeField] private string m_SelectedCamera;
	[SerializeField] private bool m_coach;
    void Awake () {
        DontDestroyOnLoad (this.transform.gameObject);
    }

    // Use this for initialization
    void Start () {
        m_SelectedCamera = "0";
    }

    // Update is called once per frame
    void Update () {

    }

    public void OnButtonSelect (string s) {
        m_SelectedCamera = s;
    }

    public string GetCamera () {
        return m_SelectedCamera;
    }
}