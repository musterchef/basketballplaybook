﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchCamera : MonoBehaviour {

    [SerializeField] private Transform[] cameras; // array di camera
    [SerializeField] private Camera campo, giocatore;
    [SerializeField] private int i; // indice camera
    [SerializeField] private Button b1, b2, b3, b4, b5; // bottoni per la scelta camera
    [SerializeField] private GameObject CameraSelect;
    [SerializeField] private Manager3D Manager3D;
    [SerializeField] private Canvas CoachCanvas;

    // Use this for initialization
    void Start () {
        i = -1; // setto camera dall'alto di default

        // aggiungo il listener sui bottoni

        b1.onClick.AddListener (() => ChangeCamera (b1.transform.GetChild (0).GetComponent<Text> ().text));
        b2.onClick.AddListener (() => ChangeCamera (b2.transform.GetChild (0).GetComponent<Text> ().text));
        b3.onClick.AddListener (() => ChangeCamera (b3.transform.GetChild (0).GetComponent<Text> ().text));
        b4.onClick.AddListener (() => ChangeCamera (b4.transform.GetChild (0).GetComponent<Text> ().text));
        b5.onClick.AddListener (() => ChangeCamera (b5.transform.GetChild (0).GetComponent<Text> ().text));

        // SETTO LA CAMERA BELLA BELLA

        CameraSelect = GameObject.Find ("CameraSelect");
        if (CameraSelect != null) {
            Manager3D.Activate3DPlayerOnClients (); // modalità 3d prima di mettere la camera
            CoachCanvas.gameObject.SetActive(false); // tolgo il canvas dell'allenatore
            CameraSelect cs = CameraSelect.GetComponent<CameraSelect> ();
            ChangeCamera (cs.GetCamera ());
        }

    }

    // Update is called once per frame
    void Update () {

    }

    public void ChangeCamera (string s) {

        int camera = Int32.Parse (s) - 1;

        i = camera;

        this.transform.position = cameras[camera].position;
        this.transform.parent = cameras[camera].transform;
        giocatore = cameras[camera].GetChild (0).GetComponent<Camera> ();
        giocatore.enabled = true;
        campo.enabled = false;
    }

}