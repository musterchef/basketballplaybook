﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Photon;
using SWS;
using TouchScript.Examples.Tap;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.Networking;

public class PlayersManager : Photon.MonoBehaviour {

    public bool home_away { get; set; }
    // public GameObject hprefab;
    // public GameObject aprefab;
    public Spawn spawn; // spawn.gameobj.transform.getchild(0) = container
    private List<Transform> AwayPlayersInTheScene;
    private List<Transform> HomePlayersInTheScene;
    private List<Transform> GhostPlayersIntheScene;
    public Dictionary<string, Transform> playermap {private set; get;}
    [SerializeField] private Ball m_Ball;
    public Ball Ball { get; set; }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake () {
        HomePlayersInTheScene = new List<Transform> ();
        AwayPlayersInTheScene = new List<Transform> ();
        GhostPlayersIntheScene = new List<Transform> ();
        playermap = new Dictionary<string, Transform>();
        Ball = m_Ball;
    }

    // Use this for initialization
    void Start () {
        home_away = true;
    }

    // Update is called once per frame
    void Update () { }

    // Select Home Team
    public void OnHomeButtonClick () {
        home_away = true;
        //        spawn.setCubePrefab (hprefab.transform);
        //spawn.setHomeAwayString(home_away);
    }

    // Select Away Team
    public void OnAwayButtonClick () {
        home_away = false;
        //        spawn.setCubePrefab (aprefab.transform);
        //spawn.setHomeAwayString(home_away);
    }

    // Add player to players list
    public void AddPlayerToHomePlayersList (Transform t) {
        HomePlayersInTheScene.Add (t);
        playermap.Add(t.name, t.GetChild(0));
    }

    // Add player to players list
    public void AddPlayerToAwayPlayersList (Transform t) {
        AwayPlayersInTheScene.Add (t);
    }

    // Add player to ghosts list
    public void AddPlayerToGhostsPlayersList (Transform t) {
        GhostPlayersIntheScene.Add (t);
    }

    // Get the list with the current situation in the court --> I can use this method to store the keyframe with the actual positioning of the players
    public List<Transform> GetHomePlayerInTheSceneList () {
        return HomePlayersInTheScene;
    }

    // Get the list with the current situation in the court --> I can use this method to store the keyframe with the actual positioning of the players
    public List<Transform> GetAwayPlayerInTheSceneList () {
        return AwayPlayersInTheScene;
    }

    // Get the list with the current ghost situation in the court --> I can use this method to store the keyframe with the actual positioning of the players
    public List<Transform> GetGhostPlayerInTheSceneList () {
        return GhostPlayersIntheScene;
    }
    // Print player list (only to check if it's storing correctly the informations in the list) maybe I'll use this later more usefully 
    public void PrintPlayersList () {
        foreach (var v in AwayPlayersInTheScene) {
            Debug.Log (v.name);
        }
    }

    public void SetHomeAwayPlayerInTheSceneList () {
        this.HomePlayersInTheScene = new List<Transform> ();
        this.AwayPlayersInTheScene = new List<Transform> ();
    }

    public void SpawnAfterLoad (Timeline t) {
        // foreach (PlayerInfo player in t.frames[0].players_home) {
        //  spawn.SpawnLoad (player.idPlayer, new Vector3 (player.x, 0f, player.z));
        // }

        for (int i = 0; i < this.HomePlayersInTheScene.Count - 1; i++) {
            this.HomePlayersInTheScene[i].position = new Vector3 (t.frames[t.frames.Count - 1].players_home[i].x, 0.49f, t.frames[t.frames.Count - 1].players_home[i].z);
        }

        for (int i = 0; i < this.AwayPlayersInTheScene.Count - 1; i++) {
            this.AwayPlayersInTheScene[i].position = new Vector3 (t.frames[t.frames.Count - 1].players_away[i].x, 0.49f, t.frames[t.frames.Count - 1].players_away[i].z);
        }

        Ball.gameObject.transform.position = t.frames[t.frames.Count - 1].ball_info;

    }

    public void EnableDefence () {
        foreach (Transform t in AwayPlayersInTheScene) {
            if(t.name.Equals("Ball") == false)
            t.gameObject.SetActive (!t.gameObject.activeInHierarchy);
        }
    }

    public void ActivatePlayers () {
        foreach (Transform t in HomePlayersInTheScene) {
            t.GetChild (0).gameObject.SetActive (true);
        }
    }

}