﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SWS;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PathCreator : MonoBehaviour {

    private WaypointManager WaypointManager;
    private AnimationManager AnimationManager;
    private PlayersManager PlayersManager;
    private TimelineManager TimelineManager;
    public GameObject ball;
    private GameObject ball_path;
    private List<PathManager> paths_list;
    public bool already_pressed;
    public bool paused { get; set; }
    public bool first_time_play = false;
    private bool first = false;

    // variabili per gestire lo slider, i know sono tante però così funziona bene
    public float start_pause_time { get; set; }
    public float stop_pause_time { get; set; }
    // public float time_elapsed_at_pause { get; set; }
    public float pause_time { get; set; }
    public float time_started_play { set; get; }
    private Slider timeline_slider;
    public float saved_timeline_value { get; set; }
    private IEnumerator coroutine;
    private Dictionary<string, Transform> pathMap;

    // public Transform player;
    [SerializeField] private Transform cloni;
    private int contatore = 0;

    // Awake is called when the script instance is being loaded.
    void Awake () {
        this.WaypointManager = this.GetComponent<WaypointManager> ();
        this.AnimationManager = GameObject.Find ("AnimationManager").GetComponent<AnimationManager> ();
        this.PlayersManager = GameObject.Find ("PlayersManager").GetComponent<PlayersManager> ();
        this.TimelineManager = GameObject.Find ("TimelineManager").GetComponent<TimelineManager> ();
        timeline_slider = GameObject.Find ("Timeline_Slider").GetComponent<Slider> ();
    }

    // Use this for initialization
    void Start () {
        already_pressed = false;
        paused = false;
        // time_elapsed_at_pause = 0f;
        pause_time = 0f;
        paths_list = new List<PathManager> ();
        pathMap = new Dictionary<string, Transform> ();
    }

    // Update is called once per frame
    void Update () {

    }
    public void OnPlayPressed () {

        // *** IMPORTANTE *** DA FARE: ---> MANCA IL CONTROLLO SE I PATH GIÀ ESISTONO. IN TAL CASO FAR PARTIRE IL PLAY e BASTA!

        Timeline timeline = AnimationManager.GetTimeline ();

        // creo i vari path da riempire con le info sui movimenti dei giocatori nei vari frame

        if (already_pressed == false) {
            int i = 0;

            // HOME

            foreach (var v in PlayersManager.GetHomePlayerInTheSceneList ()) {
                GameObject g = new GameObject (v.name);
                g.transform.parent = this.transform;
            }

            // AWAY

            foreach (var v in PlayersManager.GetAwayPlayerInTheSceneList ()) {
                GameObject g = new GameObject (v.name);
                g.transform.parent = this.transform;
            }

            // creo il GameObject che conterra il path della Ball
            ball_path = new GameObject ("Ball");
            ball_path.transform.parent = this.transform;

            foreach (Frame f in timeline.frames) {
                // home
                foreach (PlayerInfo p in f.players_home) {

                    GameObject g = new GameObject (((p.idPlayer) + "_") + i.ToString ());
                    g.transform.position = new Vector3 (p.x, 0.5f, p.z);

                    // controllo in quale dei path devo fare il salvataggio del waypoint

                    for (int j = 0; j < this.transform.childCount; j++) {
                        if (this.transform.GetChild (j).name.Equals (p.idPlayer)) {
                            g.transform.parent = this.transform.GetChild (j).transform;
                        }
                    }
                }
                // away
                foreach (PlayerInfo p in f.players_away) {

                    GameObject g = new GameObject (((p.idPlayer) + "_") + i.ToString ());
                    g.transform.position = new Vector3 (p.x, 0.5f, p.z);

                    // controllo in quale dei path devo fare il salvataggio del waypoint

                    for (int j = 0; j < this.transform.childCount; j++) {
                        if (this.transform.GetChild (j).name.Equals (p.idPlayer)) {
                            g.transform.parent = this.transform.GetChild (j).transform;
                        }
                    }
                }

                // creo il waypoint della palla a quel frame e lo metto sotto il suo path corrispondente

                GameObject ball_waypoint = new GameObject ("Ball_" + i.ToString ());
                ball_waypoint.transform.position = new Vector3 (f.ball_info.x + 0.32f, f.ball_info.y, f.ball_info.z);
                ball_waypoint.transform.parent = ball_path.transform;

                i++;
            }
            already_pressed = true;
        } else {
            int i = 0;
            foreach (Frame f in timeline.frames) {

                // HOME
                foreach (PlayerInfo p in f.players_home) {

                    // se non trovo il Waypoint lo creo e lo metto sotto al suo path corrispondente

                    if (FindWaypoint (i, p) == false) {
                        GameObject g = new GameObject (p.idPlayer + "_" + i.ToString ());
                        g.transform.position = new Vector3 (p.x, 0.5f, p.z);
                        foreach (Transform t in this.GetComponentsInChildren<Transform> ())
                            if (t.name.Equals (p.idPlayer)) {
                                g.transform.parent = t.transform;
                            }
                    }

                }

                // AWAY
                foreach (PlayerInfo p in f.players_away) {

                    // se non trovo il Waypoint lo creo e lo metto sotto al suo path corrispondente

                    if (FindWaypoint (i, p) == false) {
                        GameObject g = new GameObject (p.idPlayer + "_" + i.ToString ());
                        g.transform.position = new Vector3 (p.x, 0.5f, p.z);
                        foreach (Transform t in this.GetComponentsInChildren<Transform> ())
                            if (t.name.Equals (p.idPlayer)) {
                                g.transform.parent = t.transform;
                            }
                    }

                }
                i++;
            }
        }

        // Aggiungo il componente path manager al gameobject padre dei gameobject waypoint
        // rendo i figli di questo gameobjet i waypoint e dopo faccio partire lo start

        // PATHS

        // paths_list = new List<PathManager> ();

        paths_list.Clear ();

        for (int i = 0; i < this.transform.childCount; i++) {
            Transform t = this.transform.GetChild (i);
            PathManager pm = t.gameObject.AddComponent<PathManager> ();
            if (t.transform.childCount > 0)
                t.GetComponent<PathManager> ().Create ();
            paths_list.Add (pm);
        }

        InserisciPath ();
        first_time_play = true;
    }

    public void InserisciPath () {

        // BALL PATH
        ball.AddComponent<splineMove> ().pathContainer = ball_path.GetComponent<PathManager> ();
        ball.GetComponent<splineMove> ().pathType = DG.Tweening.PathType.Linear;

        // PLAYERS HOME PATHS
        for (int i = 0; i < 5 /*this.transform.childCount - 6*/ ; i++) {
            Debug.Log (this.transform.GetChild (i).GetComponent<PathManager> ());
            this.PlayersManager.GetHomePlayerInTheSceneList () [i].GetComponent<splineMove> ().pathContainer = this.transform.GetChild (i).GetComponent<PathManager> ();
        }

        // // PLAYERS AWAY PATHS

        // for (int i = 0; i < this.transform.childCount - 6; i++) {
        //     this.PlayersManager.GetAwayPlayerInTheSceneList ()[i].GetComponent<splineMove> ().pathContainer = this.transform.GetChild (i + 5).GetComponent<PathManager> ();
        // }
    }

    public void Play () {

        // controllo se è stato premuto play almeno una volta e provveduto a caricare i waypoint con i metodi di sopra

        if (first_time_play == false) {
            OnPlayPressed ();
        }

        if (paused == true) {

            for (int i = 0; i < 5; i++)
            {
                this.PlayersManager.GetHomePlayerInTheSceneList()[i].GetComponent<splineMove>().tween.Play();
                this.PlayersManager.GetAwayPlayerInTheSceneList()[i].GetComponent<splineMove>().tween.Play();
            }
            this.ball.GetComponent<splineMove> ().tween.Play ();

            TimelineManager.play = true; // setto true per far ripartire il cronometro
            stop_pause_time = Time.time; // salvo il tempo in cui è stato premuto pausa 
            pause_time = pause_time + (start_pause_time - stop_pause_time); // calcolo quanto tempo totale il sistema è stato in pausa 

            // rifaccio partire il cronometro

            paused = false;
        } else {
            this.PlayersManager.GetHomePlayerInTheSceneList () [0].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetHomePlayerInTheSceneList () [1].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetHomePlayerInTheSceneList () [2].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetHomePlayerInTheSceneList () [3].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetHomePlayerInTheSceneList () [4].GetComponent<splineMove> ().StartMove ();

            this.PlayersManager.GetAwayPlayerInTheSceneList () [0].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetAwayPlayerInTheSceneList () [1].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetAwayPlayerInTheSceneList () [2].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetAwayPlayerInTheSceneList () [3].GetComponent<splineMove> ().StartMove ();
            this.PlayersManager.GetAwayPlayerInTheSceneList () [4].GetComponent<splineMove> ().StartMove ();

            this.ball.GetComponent<splineMove> ().StartMove ();

            // faccio partire il cronometro

            TimelineManager.play = true;
            time_started_play = Time.time; // salvo il tempo in cui è partito play il cronometro 
            pause_time = 0f; // rimetto a zero il tempo di pausa

            // setto gli eventi

            EventsSetting ();
        }
    }

    public void Pause () {
        for (int i = 0; i < 5; i++) {
            this.PlayersManager.GetHomePlayerInTheSceneList () [i].GetComponent<splineMove> ().Pause ();
            this.PlayersManager.GetAwayPlayerInTheSceneList () [i].GetComponent<splineMove> ().Pause ();
        }
        this.ball.GetComponent<splineMove> ().Pause ();
        // metto in pausa il cronometro
        // paused = true;
        // TimelineManager.play = false;
        // start_pause_time = Time.time;
        // // time_elapsed_at_pause = start_pause_time - time_started_play;
        // saved_timeline_value = timeline_slider.value;
    }

    // metodo per andare backwards
    public void Backwards () {
        this.PlayersManager.GetHomePlayerInTheSceneList () [0].GetComponent<splineMove> ().DOPlayBackwards ();
        // this.PlayersManager.GetHomePlayerInTheSceneList ()[1].GetComponent<splineMove> ().Reverse ();
        // this.PlayersManager.GetHomePlayerInTheSceneList ()[2].GetComponent<splineMove> ().Reverse ();
        // this.PlayersManager.GetHomePlayerInTheSceneList ()[3].GetComponent<splineMove> ().Reverse ();
        // this.PlayersManager.GetHomePlayerInTheSceneList ()[4].GetComponent<splineMove> ().Reverse ();
    }

    // setto l'evento Change Speed in ogni Waypoint di ogni giocatore
    // Ora dovrei creare un metodo che prende la timeline e vede la duration da un kf all'altro e la imposta nel valore change speed

    public void EventsSetting () {

        foreach (Transform t in PlayersManager.GetHomePlayerInTheSceneList ()) {
            UnityEvent ue = new UnityEvent ();
            ue.AddListener (() => ChangeSpeed (t));
            splineMove sm = t.transform.GetComponent<splineMove> ();
            for (int j = 0; j < sm.events.Count; j++) {
                sm.events[j] = ue;
            }
        }

        foreach (Transform t in PlayersManager.GetAwayPlayerInTheSceneList ()) {
            UnityEvent ue = new UnityEvent ();
            ue.AddListener (() => ChangeSpeed (t));
            splineMove sm = t.transform.GetComponent<splineMove> ();
            for (int j = 0; j < sm.events.Count; j++) {
                sm.events[j] = ue;
            }
        }

        // BALL EVENT SETTING
        UnityEvent ueb = new UnityEvent ();
        ueb.AddListener (() => OnWaypointTouch_Ball (ball.transform));
        splineMove smb = ball.transform.GetComponent<splineMove> ();
        for (int i = 0; i < smb.events.Count; i++) {
            smb.events[i] = ueb;
        }
    }

    // Qui tutto quello che deve succedere alla PALLA al tocco di un waypoint
    // 1) cambio velocità
    // 2) passaggi
    // 3) tiri
    // 4) IN SEGUITO E DA PENSARE: tagli, blocchi, stoppate, etc...
    public void OnWaypointTouch_Ball (Transform t) {
        ChangeSpeed (t);
        DrillAndPass (t);
        // PalleggioOnWaypointTouch (t);
        // Pass2OnWaypointTouch (t);
    }

    public void DrillAndPass (Transform t) {

        splineMove splineMove = t.GetComponent<splineMove> ();
        int current_wp = splineMove.currentPoint;
        Timeline timeline = AnimationManager.gianTimeline;
        Frame f = timeline.frames[current_wp];
        List<Evento> eventi = f.event_list;
        Transform player_holder3d = PlayersManager.playermap[f.playerHolder.name];
        MoveAnimator animator = player_holder3d.GetComponent<MoveAnimator> ();
        Debug.Log ("PALLEGGIA: " + animator.name);

        ResetAnims (current_wp, animator);

        // if (current_wp == 0) {
        //     animator.SetPalleggio (true);
        //     animator.palla.GetComponent<MeshRenderer> ().enabled = true;
        // }

        foreach (Evento e in eventi) {
            // Debug.Log ("Evento: " + e.id_type + " ID 1: " + e.idPlayer1 + " ID 2: " + e.idPlayer2 + " duration: " + e.duration + " true: " + e.startend);
            if (e.id_type == "passaggio") {
                if (e.startend == true) {
                    // prendo il giocatore che effettua il passaggio
                    Transform player = PlayersManager.playermap[e.idPlayer1];
                    // prendo il suo componente che gestisce il movimento e setto i valori booleani dell'animator
                    MoveAnimator player_3D = player.GetComponent<MoveAnimator> ();
                    if (player_3D.GetPalleggio () == true) {
                        // Debug.Log ("Sto per settare il trigger di passaggio");
                        Transform start = player_3D.palla.transform;
                        MoveBall mb = player_3D.palla.GetComponent<MoveBall> ();
                        mb.parameters =
                            e.idPlayer1.ToString () + ";" +
                            e.idPlayer2.ToString () + ";" +
                            e.duration.ToString ();
                        // Debug.Log ("MB: " + mb.parameters);
                        player_3D.SetPassaggio (PlayersManager.playermap[e.idPlayer2]); // trigger passaggio
                    }
                }
            }

            if (e.id_type == "tiro") {
                Debug.Log ("ECCO IL NOSTRO QUINTO GIOCATORE");
                // prendo il giocatore che effettua il passaggio
                Transform player = PlayersManager.playermap[e.idPlayer1];
                // prendo il suo componente che gestisce il movimento e setto i valori booleani dell'animator
                MoveAnimator player_3D = player.GetComponent<MoveAnimator> ();
                Debug.Log("STO x SETTARLO");
                player_3D.SetTiro(player_3D.canestro.position, player_3D.transform.position, 1f);
            }
        }
    }

    // dalla stringa del nome mi prendo il numero del giocatore
    public int GetPlayer (Transform t) {
        // PER VECCHIA DENOMINAZIONE Player_Home_#
        // char c = id[12];
        // return Int32.Parse (c.ToString ());

        // NUOVA CON NUMERI SOLTANTO
        int to = PlayersManager.GetHomePlayerInTheSceneList ().IndexOf (t);
        Debug.Log (to.ToString ());
        return PlayersManager.GetHomePlayerInTheSceneList ().IndexOf (t);

    }

    // chiamo per ogni giocatore (al momento HOME, aggiunto AWAY) il metodo SetSpeed del componente splineMove passando:
    // 1) l'indice del WayPoint corrente perché devo calcolare la distanza tra il waypoint corrente e quello successivo
    // 2) la lista di path perché mi servirà trovare il path corrispondente al gameobject in questione
    // 3) AnimationManager --> perché mi servirà la Timeline per calcolare le differenze di tempo tra un KF e l'altro.
    public void ChangeSpeed (Transform t) {
        // Debug.Log ("CHANGE SPEED TO: " + t.name);
        t.GetComponent<splineMove> ().SetSpeed (AnimationManager);
    }

    // metodo che scorre i waypoint già esistenti e aggiorna i dati con gli ultimi inseriti nella timeline
    // ritorna il booleano found che serve per creare i waypoint aggiunti successivamente

    public bool FindWaypoint (int index_frame, PlayerInfo p) {
        bool found = false;
        for (int i = 0; i < this.transform.childCount; i++) {
            for (int j = 0; j < this.transform.GetChild (i).childCount; j++) {
                if (this.transform.GetChild (i).GetChild (j).name.Equals (p.idPlayer + "_" + index_frame.ToString ())) {
                    //Debug.Log ("J: " + j + " IND: " + index_frame);
                    this.transform.GetChild (i).GetChild (j).transform.position = new Vector3 (p.x, 0.5f, p.z);
                    found = true;
                }
            }
        }
        //Debug.Log ("FOUND IS: " + found);
        return found;
    }

    public void ResetFirstTimePlay () {
        first_time_play = false;
    }

    public void OnKeyframeAdded (Timeline timeline) {

        Timeline t = timeline;
        int i = t.frames.Count - 1;
        // creo le cartelle dei giocatori (e della palla) nei waypoints
        if (i == 0) {
            foreach (var v in AnimationManager.GetTimeline ().frames[0].players_home) {
                GameObject g = new GameObject (v.idPlayer);
                g.transform.parent = this.transform;
            }

            foreach (var v in AnimationManager.GetTimeline ().frames[0].players_away) {
                GameObject g = new GameObject (v.idPlayer);
                g.transform.parent = this.transform;
            }

            // creo il GameObject che conterra il path della Ball
            ball_path = new GameObject ("Ball");
            ball_path.transform.parent = this.transform;
        }

        foreach (PlayerInfo p in t.frames[i].players_home) {
            GameObject g = new GameObject (((p.idPlayer) + "_") + i.ToString ());
            g.transform.position = new Vector3 (p.x, p.y, p.z);
            // controllo in quale dei path devo fare il salvataggio del waypoint
            for (int j = 0; j < this.transform.childCount; j++) {
                if (this.transform.GetChild (j).name.Equals (p.idPlayer)) {
                    g.transform.parent = this.transform.GetChild (j).transform;
                }
            }
        }

        float x = t.frames[i].ball_info.x;
        float z = t.frames[i].ball_info.z;
        GameObject go = new GameObject ("Ball_" + i.ToString ());
        go.transform.position = new Vector3 (x, 0.3f, z);
        go.transform.parent = ball_path.transform;

    }

    public void OnLoadGianpaoloTimeline () {
        CreateWaypointFolder ();
        CreateWaypoints (AnimationManager.gianTimeline);
    }

    public void OnPlayGianpaolo () {

        AnimationManager.loaded = true;

        if (paused == true) {
            Debug.Log ("ERA IN PAUSA PRIMA");
            stop_pause_time = Time.time;
            pause_time = pause_time + (start_pause_time - stop_pause_time);
            paused = false;

            for (int i = 0; i < PlayersManager.GetHomePlayerInTheSceneList ().Count; i++) {
                PlayersManager.GetHomePlayerInTheSceneList () [i].GetComponent<splineMove> ().tween.Play ();
                PlayersManager.GetAwayPlayerInTheSceneList () [i].GetComponent<splineMove> ().tween.Play ();
            }
            ball.GetComponent<splineMove> ().tween.Play ();
            return;
        }

        for (int i = 0; i < 5; i++) {
            GameObject homePlayer = this.transform.GetChild(i).gameObject;
            PathManager pathManagerHome = homePlayer.GetComponent<PathManager> ();
            if (pathManagerHome == null) {
                pathManagerHome = homePlayer.AddComponent<PathManager> ();
                pathMap.Add (homePlayer.name, homePlayer.transform);
            }

            GameObject awayPlayer = this.transform.GetChild(i + 5).gameObject;
            PathManager pathManagerAway = awayPlayer.GetComponent<PathManager> ();
            if (pathManagerAway == null) {
                pathManagerAway = awayPlayer.AddComponent<PathManager> ();
            }

            pathManagerHome.Create ();
            pathManagerAway.Create ();

            splineMove splineMoveHome = PlayersManager.GetHomePlayerInTheSceneList () [i].GetComponent<splineMove> ();
            splineMoveHome.pathContainer = pathManagerHome;
            splineMoveHome.StartMove ();
        }

        splineMove splineMove = PlayersManager.GetHomePlayerInTheSceneList () [0].GetComponent<splineMove> ();

        // CALLBACKS
        splineMove.tween.OnStart (OnStart);
        splineMove.tween.OnUpdate (OnPlay);
        splineMove.tween.OnPause (OnPause);

        // EventsSetting ();

        PathManager pmb = this.transform.GetChild (10).gameObject.GetComponent<PathManager> ();
        if (pmb == null) {
            pmb = this.transform.GetChild (10).gameObject.AddComponent<PathManager> ();
        }
        pmb.Create ();
        splineMove smb = ball.GetComponent<splineMove> ();
        smb.pathContainer = pmb;
        smb.StartMove ();

        EventsSetting ();

    }

    public void OnStart () {
        time_started_play = Time.time;
        pause_time = 0f;
        // Debug.Log("START TIME AT: " + time_started_play);
        contatore = 0;
    }
    public void OnPlay () {
        float time = Time.time - time_started_play + pause_time;
        timeline_slider.value = -24f + time - TimelineManager.differenza_slide;
        // TimelineManager.OnPlay ();
        // contatore++;
        // Debug.Log("CONTATORE: " + contatore.ToString());
    }

    public void OnPause () {
        start_pause_time = Time.time;
        paused = true;
    }

    public void CreateWaypointFolder () {

        // gianpaolo edition

        foreach (var v in AnimationManager.gianTimeline.frames[0].players_home) {
            GameObject g = new GameObject (v.idPlayer);
            g.transform.parent = this.transform;
        }

        foreach (var v in AnimationManager.gianTimeline.frames[0].players_away) {
            GameObject g = new GameObject (v.idPlayer);
            g.transform.parent = this.transform;
        }

        // creo il GameObject che conterra il path della Ball
        ball_path = new GameObject ("Ball");
        ball_path.transform.parent = this.transform;
    }

    public void CreateWaypoints (Timeline timeline) {
        foreach (Frame f in timeline.frames) {
            int i = timeline.frames.IndexOf (f); // a che frame siamo?
            // HOME
            foreach (PlayerInfo p in f.players_home) {
                GameObject g = new GameObject (((p.idPlayer) + "_") + i.ToString ());
                g.transform.position = new Vector3 (p.x, 0.5f, p.z);
                // controllo in quale dei path devo fare il salvataggio del waypoint
                for (int j = 0; j < this.transform.childCount; j++) {
                    if (this.transform.GetChild (j).name.Equals (p.idPlayer)) {
                        g.transform.parent = this.transform.GetChild (j).transform;
                    }
                }
            }
            // AWAY
            foreach (PlayerInfo p in f.players_away) {
                GameObject g = new GameObject (((p.idPlayer) + "_") + i.ToString ());
                g.transform.position = new Vector3 (p.x, 0.5f, p.z);
                // controllo in quale dei path devo fare il salvataggio del waypoint
                for (int j = 0; j < this.transform.childCount; j++) {
                    if (this.transform.GetChild (j).name.Equals (p.idPlayer)) {
                        g.transform.parent = this.transform.GetChild (j).transform;
                    }
                }
            }
            // creo il waypoint della palla a quel frame e lo metto sotto il suo path corrispondente
            GameObject ball_waypoint = new GameObject ("Ball_" + i.ToString ());
            ball_waypoint.transform.position = new Vector3 (f.ball_info.x + 0.32f, f.ball_info.y, f.ball_info.z);
            ball_waypoint.transform.parent = ball_path.transform;
        }
    }

    public void RemoveKeyframe () {
        Timeline t = AnimationManager.GetTimeline ();
        Frame f = AnimationManager.IsKeyframe (timeline_slider.value);
        if (f.players_home != null) {
            // Debug.Log ("indice del frame: " + t.frames.IndexOf (f));
            int i = t.frames.IndexOf (f);
            if (i != 0) { // non voglo cancellare il primo keyframe
                TimelineManager.RemoveMarker (t.frames.IndexOf (f)); // rimuovo il marker

                // parte per togliere cloni e posizioni nel waypoint

                for (int j = 0; j < 10; j++) { // scorro tra le cartelle di waypoints
                    GameObject go = this.transform.GetChild (j).GetChild (i).gameObject;
                    GameObject.Destroy (go);
                    if (cloni.GetChild (j).childCount > 0) { // controllo se ci sono i cloni (questo succede quando ci sono i frames inseriti a mano)
                        for (int k = 0; k < cloni.GetChild (j).childCount; k++)
                            GameObject.Destroy (cloni.GetChild (j).GetChild (k).gameObject); // distruggo clone
                    }
                }
                Debug.Log ("RIMOSSO FRAME: " + f.timeStamp.ToShortDateString () + " index: " + t.frames.IndexOf (f).ToString ());
                t.frames.Remove (f); // rimuovo il frame dalla timeline
                AnimationManager.lastKeyFrameSeconds = AnimationManager.GetToday ().Subtract (t.frames[t.frames.Count - 1].timeStamp).Seconds; // sistemo l'ultimo keyframe
            }
        }

    }

    public void ResetAnims (int current_wp, MoveAnimator animator) {
        if (current_wp == 0) {
            foreach (Transform pl in PlayersManager.GetHomePlayerInTheSceneList ()) {
                MoveAnimator mov = pl.GetChild (0).GetComponent<MoveAnimator> ();
                mov.SetPalleggio (false);
                mov.palla.GetComponent<MeshRenderer> ().enabled = false;
                mov.SetLayerW(false);
            }
            animator.SetLayerW(true);
            animator.SetPalleggio (true);
            animator.DisablePhysics();
            animator.palla.GetComponent<MeshRenderer> ().enabled = true;
        }
    }

}