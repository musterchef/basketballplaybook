﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using SWS;
using UnityEngine;
using UnityEngine.UI;

    public class AnimationManager : MonoBehaviour
    {

        public int lastKeyFrameSeconds { get; set; }
        public int sliderSeconds { get; private set; }
        public bool loaded { get; set; }
        public Slider timelineSlider { get; set; }
        public Timeline gianTimeline { get; private set; }

        private Timeline timeline;
        private DateTime today;
        private float indexToGoBack;
        private bool editable;

        [SerializeField] private Text stopwatch;
        [SerializeField] private TimelineManager timelineManager;
        [SerializeField] private PlayersManager playersManager;
        [SerializeField] private Manager3D manager3d;
        [SerializeField] private EventManager eventManager;
        [SerializeField] private PathCreator pathCreator;
        [SerializeField] private Ball ballManager;
        [SerializeField] private Transform contenitore;

        // Awake is called when the script instance is being loaded.
        void Awake()
        {
            timelineSlider = GameObject.Find("Timeline_Slider").GetComponent<Slider>();
        }

        // Use this for initialization
        void Start()
        {

            loaded = false;
            editable = false;

            timeline = new Timeline();
            today = DateTime.Now;
            sliderSeconds = 24;
            lastKeyFrameSeconds = 25;
        }

        // Update is called once per frame
        void Update()
        {

        }

        // method that stores all the info for that keyframe
        public void InsertButton_Click()
        {
            // Computes timestamp
            DateTime timestamp = today.AddSeconds(timelineSlider.value);
            int index = timeline.frames.FindIndex(x => x.timeStamp.Equals(timestamp));
            if (index != -1)
            {
                // Removes duplicate frames
                timeline.frames.RemoveAt(index);
            }
            // Gets player info
            List<PlayerInfo> currentHomeFramePlayerInfo = new List<PlayerInfo>();
            List<PlayerInfo> currentAwayFramePlayerInfo = new List<PlayerInfo>();

            foreach (Transform t in playersManager.GetHomePlayerInTheSceneList())
            {
                float x2 = t.position.x;
                float z2 = t.position.z;

                // move players by 0.3f if coach doesn't move them in order to fix the bug that occurs when the difference of position between two frames is 0 
                // (todo: need to improve this)

                if (timeline.frames.Count > 0)
                {
                    float x1 = timeline.frames[timeline.frames.Count - 1].players_home[currentHomeFramePlayerInfo.Count].x;
                    float z1 = timeline.frames[timeline.frames.Count - 1].players_home[currentHomeFramePlayerInfo.Count].z;
                    if ((x1 == x2) && (z1 == z2))
                    {
                        x2 = x2 + 0.3f;
                        z2 = z2 + 0.3f;
                    }
                }
                currentHomeFramePlayerInfo.Add(new PlayerInfo(t.name, x2, z2, 0.3f));
            }

            // do the same for AWAY team

            foreach (Transform t in playersManager.GetAwayPlayerInTheSceneList())
            {

                float x2 = t.position.x;
                float z2 = t.position.z;

                if (timeline.frames.Count > 0)
                {
                    float x1 = timeline.frames[timeline.frames.Count - 1].players_home[currentAwayFramePlayerInfo.Count].x;
                    float z1 = timeline.frames[timeline.frames.Count - 1].players_home[currentAwayFramePlayerInfo.Count].z;
                    if ((x1 == x2) && (z1 == z2))
                    {
                        x2 = x2 + 0.3f;
                        z2 = z2 + 0.3f;
                    }
                }
                currentAwayFramePlayerInfo.Add(new PlayerInfo(t.name, x2, z2, 0.3f));
            }

            // manage events for this keyframe
            List<Evento> CurrentFrameEvents = new List<Evento>();
            List<Evento> PastFrameEvents = new List<Evento>();

            foreach (Evento e in eventManager.CurrentEvents.Values)
            {
                CurrentFrameEvents.Add(e);
                indexToGoBack = e.duration;
            }

            // Add info ball in a vector3
            Vector3 ball_position = new Vector3(ballManager.gameObject.transform.position.x, 0.5f, ballManager.gameObject.transform.position.z);
            // Adds current frame to the timeline
            if (lastKeyFrameSeconds > timelineSlider.value * (-1))
            {
                timeline.frames.Add(new Frame(timestamp, currentHomeFramePlayerInfo,
                    currentAwayFramePlayerInfo, ball_position,
                    CurrentFrameEvents, ballManager.playerHolder));

                // store the seconds on the shot_clock of the last keyframe saved
                lastKeyFrameSeconds = today.Subtract(timeline.frames[timeline.frames.Count - 1].timeStamp).Seconds;

                timeline.AddEventStartInTheEventList(CurrentFrameEvents, timestamp, today, indexToGoBack);
                // reset list in order to have only the events for this keyframe
                eventManager.ResetList();

                // add marker on the timeline
                timelineManager.AddMarker();

                // line renderer updating part

                int frame = timeline.frames.Count;

                foreach (Transform t in playersManager.GetHomePlayerInTheSceneList())
                {
                    t.GetComponent<LineRenderer>().enabled = false;
                    OnTransform onTransform = t.GetComponent<OnTransform>();
                    onTransform.OnInsertKeyframe(frame);
                }

                ballManager.GetComponent<OnTransform>().OnInsertKeyframe(frame);
                ballManager.SetLineRenderer();

                ForwardButton_Click();

                pathCreator.OnKeyframeAdded(this.timeline);
            }
            else
            {
                Debug.Log("ERRORE: METTI IL KF SEMPRE DOPO L'ULTIMO");
            }

        }

        public void OnInsertKeyframes()
        {
            string path = Environment.CurrentDirectory;
            string[] lines = System.IO.File.ReadAllLines(path + @"/azione2.txt");

            // Timeline timelineGian = new Timeline ();
            string[] line1 = lines[0].Split(',');
            float time_0 = float.Parse(line1[5]); //519.57f;
            gianTimeline = new Timeline();
            List<PlayerInfo> listaHome = new List<PlayerInfo>();
            List<PlayerInfo> listaAway = new List<PlayerInfo>();
            Vector3 ball_info = new Vector3();

            // DateTime today = DateTime.Today;

            int j = 0;

            for (int i = 0; i < lines.Length; i++)
            {

                string[] line = lines[i].Split(',');
                string s = line[0];

                switch (s)
                {
                    case "1610612750":
                        PlayerInfo ph = new PlayerInfo(line[1], ReMapValues(line[2], "x"), ReMapValues(line[3], "z"), 0.3f);
                        listaHome.Add(ph);
                        j++;
                        break;
                    case "1610612759":
                        PlayerInfo pa = new PlayerInfo(line[1], ReMapValues(line[2], "x"), ReMapValues(line[3], "z"), 0.3f);
                        listaAway.Add(pa);
                        j++;
                        break;
                    case "-1":
                        ball_info = new Vector3(ReMapValues((line[2]), "x"), 0.3f, ReMapValues((line[3]), "z"));
                        j++;
                        break;
                    default:
                        Debug.Log("CIAO");
                        break;
                }

                if (j == 11)
                {
                    DateTime timestamp = today.AddSeconds(-24 + time_0 - float.Parse(line[5]));

                    List<PlayerInfo> h = new List<PlayerInfo>();
                    List<PlayerInfo> a = new List<PlayerInfo>();
                    Vector3 b = new Vector3(ball_info.x, ball_info.y, ball_info.z);

                    foreach (PlayerInfo pi in listaHome)
                    {
                        h.Add(new PlayerInfo(pi.idPlayer, pi.x, pi.z, 0));
                    }

                    foreach (PlayerInfo pi in listaAway)
                    {
                        a.Add(new PlayerInfo(pi.idPlayer, pi.x, pi.z, 0));
                    }

                    Frame f = new Frame(timestamp, h, a, b, new List<Evento>(), contenitore.GetChild(4));
                    gianTimeline.frames.Add(f);
                    listaHome.Clear();
                    listaAway.Clear();
                    j = 0;
                }

            }

            // qui inserisco gli eventi del ML di Gian nella timeline appena riempita, brutus ma funziona

            string[] lines_eventi = System.IO.File.ReadAllLines(path + @"/azione2_eventi.txt");
            for (int i = 0; i < lines_eventi.Length; i++)
            {

                string[] line = lines_eventi[i].Split(',');
                int index = int.Parse(line[5]);
                string tipo_evento = line[0];
                string one = line[1];
                string two = line[2];
                float shot_clock = float.Parse(line[3]);
                float duration = float.Parse(line[4]);
                Evento e = new Evento(tipo_evento, i, true, one, two, duration, index);
                Debug.Log(e.idPlayer1);

                for (int z = index; z < gianTimeline.frames.Count; z++)
                {
                    gianTimeline.frames[z].playerHolder = GameObject.Find(two).transform;
                }
                gianTimeline.frames[index - 1].event_list.Add(e);
            }
            loaded = true;
        }

        public void LoadG()
        {

            OnInsertKeyframes();
            timelineManager.LoadTimeline();
        }

        private Transform GetPlayerFromString(string s)
        {
            for (int i = 0; i < contenitore.childCount; i++)
            {
                //Debug.Log ("S:" + s);
                //Debug.Log ("GETC: " + contenitore.GetChild (i).name);
                if (String.Compare(contenitore.GetChild(i).name, s) == 0)
                {
                    //Debug.Log ("TROVATO: " + s);
                    return contenitore.GetChild(i);
                }
            }
            return null;
        }

        public void FinishButton_Click() // metodo per creare il file .csv
        {

            // COMMENTATA LA PARTE PER CREARE IL FILE .CSV CHE PER ORA NON MI SERVE!

            //string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
            string path = Environment.CurrentDirectory;

            // Write the timeline rows to a new file
            using (StreamWriter outputFile = new StreamWriter(path + @"/Tatics.csv"))
            {
                string row = string.Empty;
                foreach (Frame currentFrame in gianTimeline.frames)
                {
                    foreach (PlayerInfo currentPlayerInfo in currentFrame.players_home)
                    {
                        outputFile.WriteLine("Home" + "," + // team_id
                            currentPlayerInfo.idPlayer + "," + // player_id
                            currentPlayerInfo.x.ToString() + "," + // x_loc
                            currentPlayerInfo.z.ToString() + "," + // y_loc
                            "0" + "," + // radius (0 is temporary value, need to think about it)
                            today.Subtract(currentFrame.timeStamp).ToString() + "," + // game_clock (for the moment game and shot clock are the same)
                            today.Subtract(currentFrame.timeStamp).ToString() + "," + // shot_clock (for the moment game and shot clock are the same)
                            "0" + "," + // game_id
                            "0"); // event_id

                        // This is how a csv from NBA SportVU comes formatted, I need to manage:
                        // 1) that the coach is going to show match situations, so maybe the game_clock is useless and only the shot_clock is useful (so maybe they can be the same)
                        // 2) same reasonings for game_id and event_id (the same reason) and for the radius (that is used for the ball)

                    }

                    foreach (PlayerInfo currentPlayerInfo in currentFrame.players_away)
                    {
                        outputFile.WriteLine("Away" + "," +
                            currentPlayerInfo.idPlayer + "," +
                            currentPlayerInfo.x.ToString() + "," +
                            currentPlayerInfo.z.ToString() + "," +
                            "0" + "," +
                            today.Subtract(currentFrame.timeStamp).ToString() + "," +
                            today.Subtract(currentFrame.timeStamp).ToString() + "," +
                            "0" + "," +
                            "0");
                    }

                    outputFile.WriteLine("Ball" + "," +
                        "-1" + "," +
                        currentFrame.ball_info.x.ToString() + "," +
                        currentFrame.ball_info.y.ToString() + "," +
                        currentFrame.ball_info.z.ToString() + "," +
                        today.Subtract(currentFrame.timeStamp).ToString() + "," +
                        today.Subtract(currentFrame.timeStamp).ToString() + "," +
                        "0" + "," +
                        "HOLDER:" + currentFrame.playerHolder);

                    foreach (Evento ev in currentFrame.event_list)
                    {
                        outputFile.WriteLine("EVENT" + "," +
                            ev.id_type.ToString() + "," +
                            ev.id_event.ToString() + "," +
                            ev.startend.ToString() + "," +
                            ev.idPlayer1.ToString() + "," +
                            ev.idPlayer2.ToString() + "," +
                            ev.duration.ToString() + ", keyframe: " +
                            ev.keyframe.ToString());
                    }
                    eventManager.ResetList(); // resetto la lista così poi ho solo gli eventi per il frame in questione
                }
            }
            //path_creator.OnPlayPressed ();
        }

        public void ForwardButton_Click()
        {
            timelineSlider.value += 1;
            sliderSeconds = (int)Math.Abs(timelineSlider.value);
        }

        public void BackwardButton_Click()
        {
            timelineSlider.value -= 1;
            sliderSeconds = (int)Math.Abs(timelineSlider.value);
        }

        public void OnValueChanged()
        {

            // AGGIORNO IL CRONOMETRO

            stopwatch.text = (-timelineSlider.value).ToString("F1");

            // Cerco nella timeline se c'è il KF a quel tempo con posizioni salvati

            if ((timelineManager.play == false) && (pathCreator.already_pressed == false))
            {
                sliderSeconds = (int)Math.Abs(timelineSlider.value);
                Frame frame = IsKeyframe(timelineSlider.value);
                //faccio un passaggio in più per evitare di leggere qualcosa di null // FUNZIONA PERÒ CERCA DI SISTEMARE
                if (frame.players_home != null)
                {
                    ReadKeyframe(frame);
                }
            }

            // aggiorno il tempo

            if ((pathCreator.paused == true) && (pathCreator.already_pressed == true))
            {
                foreach (Transform t in playersManager.GetHomePlayerInTheSceneList())
                {
                    t.GetComponent<splineMove>().SlideTime(timelineSlider, this.lastKeyFrameSeconds);
                    timelineManager.differenza_slide = pathCreator.saved_timeline_value - timelineSlider.value;
                    Debug.Log("DIFFERENZA ---> " + timelineManager.differenza_slide);
                }
            }

            // controllo se c'è qualche evento già salvato e in caso di spostamento dello slider, si aggiornano i tempi correttamente

            if (eventManager.CurrentEvents.Count > 0)
            {
                foreach (Evento e in eventManager.CurrentEvents.Values)
                {
                    if (e.keyframe == this.timeline.frames.Count)
                    {
                        e.duration = this.sliderSeconds - this.lastKeyFrameSeconds;
                        Debug.Log("ID: " + e.idPlayer1 + " Duration: " + e.duration);
                    }
                }
            }

        }

        // method to read the player info from the frame and assing the positions to the players in the pitch
        public void ReadKeyframe(Frame f)
        {

            int frameIndex = timeline.frames.IndexOf(f);

            // ball position ***VECCHIO METODO FRAME BASED***
            // ball_manager.transform.position = new Vector3 (f.ball_info.x, 0.5f, f.ball_info.z);

            // ball position ***CLONE BASED***
            if (loaded == false)
            {
                Debug.Log("SONO ENTRATO Qui");
                ballManager.gameObject.GetComponent<OnTransform>().OnReadKeyFrame(frameIndex);

                foreach (Transform player in playersManager.GetHomePlayerInTheSceneList())
                {

                    // metodo vecchio basato sulla lettura della timeline

                    // foreach (PlayerInfo pi in f.players_home) {
                    //     if (player.name.Equals (pi.idPlayer))
                    //         // players positions
                    //         player.gameObject.transform.position = new Vector3 (pi.x, 0.5f, pi.z);
                    // }

                    // ****** METODO NUOVO: BASATO SUI CLONI
                    // parte per accendere o spengere il LineRenderer
                    player.GetComponent<OnTransform>().OnReadKeyFrame(frameIndex);
                    // end
                }
            }

            if (editable == true)
            {
                foreach (Transform player in playersManager.GetHomePlayerInTheSceneList())
                {

                    // metodo vecchio basato sulla lettura della timeline

                    foreach (PlayerInfo pi in f.players_home)
                    {
                        if (player.name.Equals(pi.idPlayer))
                            // players positions
                            player.gameObject.transform.position = new Vector3(pi.x, 0.5f, pi.z);
                    }
                }

            }
        }

        // method to see if a specific time in the timeline has been or not saved as a keyframe
        public Frame IsKeyframe(float timeline_slider_value)
        {
            Frame keyframe = new Frame(new DateTime(), null, null, new Vector3(), null, null);
            foreach (Frame frame in timeline.frames)
            {
                if (today.Subtract(frame.timeStamp).Seconds == (int)Math.Abs(timeline_slider_value))
                {
                    keyframe = frame;
                }
            }
            return keyframe;
        }

        public Timeline GetTimeline()
        {
            return timeline;
        }

        public void SetTimeline(Timeline timeline)
        {
            this.timeline = timeline;
            lastKeyFrameSeconds = today.Subtract(timeline.frames[timeline.frames.Count - 1].timeStamp).Seconds;
            timelineSlider.value = (-1) * (today.Subtract(timeline.frames[timeline.frames.Count - 1].timeStamp).Seconds - 1);
        }

        public DateTime GetToday()
        {
            return today;
        }

        public float TimeForThatWaypoint(int waypoint_index)
        {
            return Math.Abs((today.Subtract(this.timeline.frames[waypoint_index].timeStamp)).Seconds);
        }

        public float TimeForThatFrame(Frame f)
        {
            float t = Math.Abs((today.Subtract(f.timeStamp).Seconds));
            return t;
        }

        public Vector3 GetPositionForThatWaypointAndPlayer(int i, int c)
        {
            // Transform player = this.players_manager.GetHomePlayerInTheSceneList()[i];
            Transform path = pathCreator.transform.GetChild(i);
            Debug.Log("return: " + path.GetChild(c).name);
            return path.GetChild(c).position;
        }

        public float ReMapValues(string s, string xz)
        {
            float f = float.Parse(s);
            switch (xz)
            {
                case "x":
                    return Map(-9.7886f, 8.16f, 2.41f, 92.92f, f); //return Map (-8.955f, 8.955f, 2.41f, 92.92f, f); 9.7886
                case "z":
                    return Map(-4.8f, 4.8f, -49.36f, -0.34f, f);
                default:
                    Debug.Log("ERROR");
                    return f;
            }
        }

        public float Map(float from, float to, float from2, float to2, float value)
        {
            if (value <= from2)
            {
                return from;
            }
            else if (value >= to2)
            {
                return to;
            }
            else
            {
                return (to - from) * ((value - from2) / (to2 - from2)) + from;
            }
        }

        public void SetEditable()
        {
            editable = !editable;
            manager3d.CallEditOnClients();
        }
}