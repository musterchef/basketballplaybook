%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Braccia_Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: joint1
    m_Weight: 0
  - m_Path: P
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Body
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Bottoms
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_ChestEndEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_ChestOriginEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_HeadEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_LeftUpLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_LeftUpLeg/GiocatoreDEF:Character1_Ctrl_LeftLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_LeftUpLeg/GiocatoreDEF:Character1_Ctrl_LeftLeg/GiocatoreDEF:Character1_Ctrl_LeftFoot
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_LeftUpLeg/GiocatoreDEF:Character1_Ctrl_LeftLeg/GiocatoreDEF:Character1_Ctrl_LeftFoot/GiocatoreDEF:Character1_Ctrl_LeftToeBase
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_RightUpLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_RightUpLeg/GiocatoreDEF:Character1_Ctrl_RightLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_RightUpLeg/GiocatoreDEF:Character1_Ctrl_RightLeg/GiocatoreDEF:Character1_Ctrl_RightFoot
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_RightUpLeg/GiocatoreDEF:Character1_Ctrl_RightLeg/GiocatoreDEF:Character1_Ctrl_RightFoot/GiocatoreDEF:Character1_Ctrl_RightToeBase
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandIndex1
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandIndex1/GiocatoreDEF:Character1_Ctrl_LeftHandIndex2
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandIndex1/GiocatoreDEF:Character1_Ctrl_LeftHandIndex2/GiocatoreDEF:Character1_Ctrl_LeftHandIndex3
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandIndex1/GiocatoreDEF:Character1_Ctrl_LeftHandIndex2/GiocatoreDEF:Character1_Ctrl_LeftHandIndex3/GiocatoreDEF:Character1_Ctrl_LeftHandIndex4
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandThumb1
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandThumb1/GiocatoreDEF:Character1_Ctrl_LeftHandThumb2
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandThumb1/GiocatoreDEF:Character1_Ctrl_LeftHandThumb2/GiocatoreDEF:Character1_Ctrl_LeftHandThumb3
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_LeftShoulder/GiocatoreDEF:Character1_Ctrl_LeftArm/GiocatoreDEF:Character1_Ctrl_LeftForeArm/GiocatoreDEF:Character1_Ctrl_LeftHand/GiocatoreDEF:Character1_Ctrl_LeftHandThumb1/GiocatoreDEF:Character1_Ctrl_LeftHandThumb2/GiocatoreDEF:Character1_Ctrl_LeftHandThumb3/GiocatoreDEF:Character1_Ctrl_LeftHandThumb4
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_Neck
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_Neck/GiocatoreDEF:Character1_Ctrl_Head
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandIndex1
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandIndex1/GiocatoreDEF:Character1_Ctrl_RightHandIndex2
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandIndex1/GiocatoreDEF:Character1_Ctrl_RightHandIndex2/GiocatoreDEF:Character1_Ctrl_RightHandIndex3
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandIndex1/GiocatoreDEF:Character1_Ctrl_RightHandIndex2/GiocatoreDEF:Character1_Ctrl_RightHandIndex3/GiocatoreDEF:Character1_Ctrl_RightHandIndex4
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandThumb1
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandThumb1/GiocatoreDEF:Character1_Ctrl_RightHandThumb2
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandThumb1/GiocatoreDEF:Character1_Ctrl_RightHandThumb2/GiocatoreDEF:Character1_Ctrl_RightHandThumb3
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_Hips/GiocatoreDEF:Character1_Ctrl_Spine/GiocatoreDEF:Character1_Ctrl_Spine1/GiocatoreDEF:Character1_Ctrl_Spine2/GiocatoreDEF:Character1_Ctrl_RightShoulder/GiocatoreDEF:Character1_Ctrl_RightArm/GiocatoreDEF:Character1_Ctrl_RightForeArm/GiocatoreDEF:Character1_Ctrl_RightHand/GiocatoreDEF:Character1_Ctrl_RightHandThumb1/GiocatoreDEF:Character1_Ctrl_RightHandThumb2/GiocatoreDEF:Character1_Ctrl_RightHandThumb3/GiocatoreDEF:Character1_Ctrl_RightHandThumb4
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_HipsEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftAnkleEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftElbowEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftFootEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftHandIndexEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftHandThumbEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftHipEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftKneeEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftShoulderEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_LeftWristEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightAnkleEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightElbowEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightFootEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightHandIndexEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightHandThumbEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightHipEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightKneeEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightShoulderEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Character1_Ctrl_Reference/GiocatoreDEF:Character1_Ctrl_RightWristEffector
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Eyelashes
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Eyes
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:LeftUpLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:LeftUpLeg/GiocatoreDEF:mixamorig:LeftLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:LeftUpLeg/GiocatoreDEF:mixamorig:LeftLeg/GiocatoreDEF:mixamorig:LeftFoot
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:LeftUpLeg/GiocatoreDEF:mixamorig:LeftLeg/GiocatoreDEF:mixamorig:LeftFoot/GiocatoreDEF:mixamorig:LeftToeBase
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:LeftUpLeg/GiocatoreDEF:mixamorig:LeftLeg/GiocatoreDEF:mixamorig:LeftFoot/GiocatoreDEF:mixamorig:LeftToeBase/GiocatoreDEF:mixamorig:LeftToe_End
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:RightUpLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:RightUpLeg/GiocatoreDEF:mixamorig:RightLeg
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:RightUpLeg/GiocatoreDEF:mixamorig:RightLeg/GiocatoreDEF:mixamorig:RightFoot
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:RightUpLeg/GiocatoreDEF:mixamorig:RightLeg/GiocatoreDEF:mixamorig:RightFoot/GiocatoreDEF:mixamorig:RightToeBase
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:RightUpLeg/GiocatoreDEF:mixamorig:RightLeg/GiocatoreDEF:mixamorig:RightFoot/GiocatoreDEF:mixamorig:RightToeBase/GiocatoreDEF:mixamorig:RightToe_End
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandIndex1
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandIndex1/GiocatoreDEF:mixamorig:LeftHandIndex2
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandIndex1/GiocatoreDEF:mixamorig:LeftHandIndex2/GiocatoreDEF:mixamorig:LeftHandIndex3
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandIndex1/GiocatoreDEF:mixamorig:LeftHandIndex2/GiocatoreDEF:mixamorig:LeftHandIndex3/GiocatoreDEF:mixamorig:LeftHandIndex4
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandThumb1
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandThumb1/GiocatoreDEF:mixamorig:LeftHandThumb2
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandThumb1/GiocatoreDEF:mixamorig:LeftHandThumb2/GiocatoreDEF:mixamorig:LeftHandThumb3
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:LeftShoulder/GiocatoreDEF:mixamorig:LeftArm/GiocatoreDEF:mixamorig:LeftForeArm/GiocatoreDEF:mixamorig:LeftHand/GiocatoreDEF:mixamorig:LeftHandThumb1/GiocatoreDEF:mixamorig:LeftHandThumb2/GiocatoreDEF:mixamorig:LeftHandThumb3/GiocatoreDEF:mixamorig:LeftHandThumb4
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:Neck
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:Neck/GiocatoreDEF:mixamorig:Head
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:Neck/GiocatoreDEF:mixamorig:Head/GiocatoreDEF:mixamorig:HeadTop_End
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:Neck/GiocatoreDEF:mixamorig:Head/GiocatoreDEF:mixamorig:LeftEye
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:Neck/GiocatoreDEF:mixamorig:Head/GiocatoreDEF:mixamorig:RightEye
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandIndex1
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandIndex1/GiocatoreDEF:mixamorig:RightHandIndex2
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandIndex1/GiocatoreDEF:mixamorig:RightHandIndex2/GiocatoreDEF:mixamorig:RightHandIndex3
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandIndex1/GiocatoreDEF:mixamorig:RightHandIndex2/GiocatoreDEF:mixamorig:RightHandIndex3/GiocatoreDEF:mixamorig:RightHandIndex4
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandThumb1
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandThumb1/GiocatoreDEF:mixamorig:RightHandThumb2
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandThumb1/GiocatoreDEF:mixamorig:RightHandThumb2/GiocatoreDEF:mixamorig:RightHandThumb3
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:mixamorig:Hips/GiocatoreDEF:mixamorig:Spine/GiocatoreDEF:mixamorig:Spine1/GiocatoreDEF:mixamorig:Spine2/GiocatoreDEF:mixamorig:RightShoulder/GiocatoreDEF:mixamorig:RightArm/GiocatoreDEF:mixamorig:RightForeArm/GiocatoreDEF:mixamorig:RightHand/GiocatoreDEF:mixamorig:RightHandThumb1/GiocatoreDEF:mixamorig:RightHandThumb2/GiocatoreDEF:mixamorig:RightHandThumb3/GiocatoreDEF:mixamorig:RightHandThumb4
    m_Weight: 1
  - m_Path: P/GiocatoreDEF:Shoes
    m_Weight: 0
  - m_Path: P/GiocatoreDEF:Tops
    m_Weight: 0
  - m_Path: P/palla
    m_Weight: 0
